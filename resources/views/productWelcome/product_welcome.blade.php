@extends('layouts.app')

@section('content')

<div class="container">
  <div class="links">
    <a href="/product"><button type="button" src="/product" class="btn btn-primary btn-lg btn-block">Product Manage</button></a>
    <br>
    <a href="/"><button type="button" src="/" class="btn btn-danger btn-lg btn-block">Check order</button></a>
  </div>
  <div class="row justify-content-md-center">
    @foreach($products as $product)
                <div class="card col-md-3" style="padding: 10px; margin: 10px">
                    <div class="card-body">
                        <img src="{{ $product->url_image }}" class="card-img-top" alt="image">
                        <p class="card-text">{{ $product->name }}</p>
                        <p class="card-text">ราคา {{ $product->price }} บาท</p>
                        <center><button type="button" class="btn btn-dark">Add to cart</button></center>
                    </div>
                </div>
    @endforeach
  </div>
</div>

@endsection
