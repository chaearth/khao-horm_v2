<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><center><b><h2>Add a new product</h2></b></center></div> <br> 
                  <div class="card-body">
                    <form action="{{ route('product.create') }}">
                      <div class="form-group">
                        <label for="name">Product name:</label>
                        <input type="text" class="form-control" id="name" aria-describedby="name" placeholder="Input here..">
                      </div>
                      <div class="form-group">
                        <label for="author">Price:</label>
                        <input type="text" class="form-control" id="author" aria-describedby="author" placeholder="Input here..">
                      </div>
                      <div class="form-group">
                        <label for="author">Url image:</label>
                        <input type="text" class="form-control" id="Price" aria-describedby="Price" placeholder="Input here..">
                      </div>
                      <div align="right">
                        <a href="{{ route('product.page') }}" class="btn btn-danger">Back</a>
                        <a href="{{ route('product.create') }}" class="btn btn-primary">Save</a>
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
