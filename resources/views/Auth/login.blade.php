<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        

        <!-- Fonts -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <!-- Styles -->
        <style>
        
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }
            .bg {
  /* The image used */
            background-image: url("https://images.pexels.com/photos/1048023/pexels-photo-1048023.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260");
           

            /* Half height */
            height: 100%;
            
          /* Center and scale the image nicely */
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            background-attachment: fixed;
            }
            .position {
              left: 350px;
              top: 50px;
              height: 60vh;
            }

            .mdb-color-darker-hover {
            color: #59698D;
            -webkit-transition: .4s;
            transition: .4s; }
            .cyan-lighter-hover:hover {
              -webkit-transition: .4s;
              transition: .4s;
              color: #22e6ff; }
        </style>
    </head>
    
    <body>
        <div class="bg">
            <div class="container">
              <div class= "col">
              <br><br><br><br><br><br><br>
                 <div class="card mb-3">
                     <div class="card-body bg-dark">
                        <form action="{{route('login')}}" method="post">
                            @csrf
                            <div class="form-group" >
                                <label for="exampleInputEmail1"><font color="white">Username</font></label>
                                <input type="username" name="username" class="form-control" id="username" aria-describedby="emailHelp">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1"><font color="white">Password</font></label>
                                <input type="password" name="password" class="form-control" id="exampleInputPassword1">
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>        
    </div>
    </body>
</html>
