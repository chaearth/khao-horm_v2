<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\GuzzleException;

class OrderController extends Controller
{
    public function showOrders(){
       return view('order/orders');
    }

    public function confirmOrder($email)
    {
        $info = $this->checkOneChatUser($email);
//        if ($info->status === 'fail') {
//            return redirect()->back()->with(['status' => 'Fail!']);
//        }
//        $onechat = new Onechat();
//        $onechat->one_mail = 'jirayuph@one.th';
//        $onechat->onechat_id = $info->friend->user_id;
//        $onechat->user_id = Auth::user()->id;

//        dd($onechat);

//        if (!$onechat->save()) {
//            return redirect()->back()->with(['status' => 'Save Fail!']);
//        }
        $this->sendMessage('สวัสดี คุณได้ทำรายการสั่งชื้อ : ', $info->friend->user_id);
    }

    private function checkOneChatUser($email)
    {
        try {
            $data = [
                'headers' => [
                    'Authorization' => 'Bearer A5ae96c63a6225f32bf5e7ccb9145fc8a62cbec3e05b34df2a58261a0f027017987c0da94506341a2a5fba9ac04a9eef4',
                    'Content-Type' => 'application/json',
                ],
                'json' => [
                    'bot_id' => 'B3ec27f63d1c754299c6fa962af638328',
                    'key_search' => $email
                ],
            ];

            $client = new Client();
            $res = $client->request('POST', 'https://chat-manage.one.th:8997/api/v1/searchfriend',
                $data
            );
            $resToJson = json_decode($res->getBody()->getContents());
            return $resToJson;
        } catch (ConnectException $e) {
            return (object)['status' => 'fail'];
        }
    }

    private function sendMessage($msg, $onechat_id)
    {
        try {
            $client = new Client();
            $res = $client->request('POST', "https://chat-public.one.th:8034/api/v1/push_message", [
                "headers" => [
                    'Authorization' => "Bearer A5ae96c63a6225f32bf5e7ccb9145fc8a62cbec3e05b34df2a58261a0f027017987c0da94506341a2a5fba9ac04a9eef4",
                    "Content-Type" => "application/json",
                ],
                'json' => [
                    'to' => $onechat_id,
                    'bot_id' => "B3ec27f63d1c754299c6fa962af638328",
                    'type' => 'text',
                    "message" => $msg,
                ]
            ]);
            return null;
        } catch (GuzzleException $e) {
            return null;
        }
    }
}
