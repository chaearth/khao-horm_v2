<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{

    public function getProduct() {
//        dd(1);
        $productName = product::all();
        return view('productWelcome.product_welcome', ['products' => $productName]);
    }

    public function index()
    {
        $products = product::all();

        return view('product.index', ['products' => $products]);
    }

    public function createPage()
    {
        return view('product.create');
    }
    
    public function create(Request $request)
    {
        $product = new Product();
        $product->name = $request->name;
        $product->price = $request->price;
        $product->url_image = $request->url_image;

        if (!$product->save()) {
            return redirect()->route('product.create.page');
        }
        return redirect()->route('product.page');
    }

    public function getProduct2() {
        //        dd(1);
                $productName = product::all();
                return view('productWelcome.product_user',['products' => $productName]);
            }

    public function createProduct(Request $request)
    {
        $product = new product();
        $product->name = $request->name;
        $product->price = $request->price;
        $product->url_image = $request->url_image;
        if (!$product->save()){
            return redirect()->json([
                'status' => false,
            ]);
        }
        return response()->json([
            'status' => true,
            'product' => $product,
        ]);
    }

    public function cart()
    {
        return view('productWelcome.cart');
    }
    public function addToCart($id)
    {
        $product = Product::find($id);

        if(!$product) {

            abort(404);

        }

        $cart = session()->get('cart');

        // if cart is empty then this the first product
        if(!$cart) {

            $cart = [
                $id => [
                    "name" => $product->name,
                    "quantity" => 1,
                    "price" => $product->price
                ]
            ];

            session()->put('cart', $cart);

            $htmlCart = view('_header_cart')->render();

            return response()->json(['msg' => 'Product added to cart successfully!', 'data' => $htmlCart]);

            //return redirect()->back()->with('success', 'Product added to cart successfully!');
        }

        // if cart not empty then check if this product exist then increment quantity
        if(isset($cart[$id])) {

            $cart[$id]['quantity']++;

            session()->put('cart', $cart);

            $htmlCart = view('_header_cart')->render();

            return response()->json(['msg' => 'Product added to cart successfully!', 'data' => $htmlCart]);

            //return redirect()->back()->with('success', 'Product added to cart successfully!');

        }

        // if item not exist in cart then add to cart with quantity = 1
        $cart[$id] = [
            "name" => $product->name,
            "quantity" => 1,
            "price" => $product->price
        ];

        session()->put('cart', $cart);

        $htmlCart = view('_header_cart')->render();

        return response()->json(['msg' => 'Product added to cart successfully!', 'data' => $htmlCart]);

        //return redirect()->back()->with('success', 'Product added to cart successfully!');
    }

    public function update(Request $request)
    {
        if($request->id and $request->quantity)
        {
            $cart = session()->get('cart');

            $cart[$request->id]["quantity"] = $request->quantity;

            session()->put('cart', $cart);

            $subTotal = $cart[$request->id]['quantity'] * $cart[$request->id]['price'];

            $total = $this->getCartTotal();

            $htmlCart = view('_header_cart')->render();

            return response()->json(['msg' => 'Cart updated successfully', 'data' => $htmlCart, 'total' => $total, 'subTotal' => $subTotal]);

            //session()->flash('success', 'Cart updated successfully');
        }
    }

    public function remove(Request $request)
    {
        if($request->id) {

            $cart = session()->get('cart');

            if(isset($cart[$request->id])) {

                unset($cart[$request->id]);

                session()->put('cart', $cart);
            }

            $total = $this->getCartTotal();

            $htmlCart = view('_header_cart')->render();

            return response()->json(['msg' => 'Product removed successfully', 'data' => $htmlCart, 'total' => $total]);

            //session()->flash('success', 'Product removed successfully');
        }
    }


    /**
     * getCartTotal
     *
     *
     * @return float|int
     */
    private function getCartTotal()
    {
        $total = 0;

        $cart = session()->get('cart');

        foreach($cart as $id => $details) {
            $total += $details['price'] * $details['quantity'];
        }

        return number_format($total, 2);
    }

    public function editPage($id)
    {
        $product = product::find($id);
        return view('product.edit', compact('product'));
    }

    public function edit(Request $request)
    {
        $product = product::find($request->id);
        $product->name = $request->name;
        $product->price = $request->price;
        $product->url_image = $request->url_image;

        if (!$product->save()) {
         return redirect()->route('product.edit.page');
        }

        return redirect()->route('product.page');
    }

    public function deletePage($id)
    {
        $product = product::find($id);
        $product->delete();
        return redirect()->route('product.page');
    }
}

