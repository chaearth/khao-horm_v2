<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\RegisterRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Onechat;

class UserController extends Controller
{
    public function registerPage()
    {
        return view('Auth.register');
    }

    public function register(RegisterRequest $request)
    {
        $userExist = User::where('email', $request->email)->exists();

        if ($userExist) {
            return redirect()->back()->with(['status' => 'user is already existing !!']);
        }

        $user = new user();

        $user->username = $request->username;
        $user->password = Hash::make($request->password);
        $user->email = $request->email;
        $user->tel = $request->tel;

        if (!$user->save()) {
            return redirect()->with('success', 'Item created successfully!')->back();
        }

        return redirect()->route('login');
    }

    public function loginPage()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $isAuth = Auth::attempt([
            'username' => $request->username,
            'password' => $request->password,
        ]);

        if (!$isAuth) {
            return redirect()->back()->with(['status' => 'Login failed !!']);
        }

        return redirect('/');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

    public function profile()
    {
        $user = Auth::user()->onechat();
        dd($user);
        return view('auth.profile', compact('user'));
    }

    public function storeOneChat(Request $request)
    {
        $info = $this->checkOneChatUser($request->one_mail);
        if ($info->status === 'fail') {
            return redirect()->back()->with(['status' => 'Fail!']);
        }

        $onechat = new Onechat();
        $onechat->one_mail = $request->one_mail;
        $onechat->onechat_id = $info->friend->user_id;
        $onechat->user_id = Auth::user()->id;

        if (!$onechat->save()) {
            return redirect()->back()->with(['status' => 'Save Fail!']);
        }
        $this->sendMessage('สวัสดี คุณได้ทำการแก้ไขโปรไฟล์เรียบร้อยแล้ว',  $info->friend->user_id);
        return redirect()->back();
    }

    private function checkOneChatUser($email)
    {
    try {
        $client = new Client();
        $res = $client->request('POST', "https://chat-manage.one.th:8997/api/v1/searchfriend", [
            "headers" => [
                'Authorization' => "Bearer A34b3653754575acb8358883ba2df6e56e81eae7b4cc14c1d9d773f79e24a00a59fd5fb165e0a422aaea80d0eea2e990b",
                "Content-Type" => "application/json",
            ],
            'json' => [
                'bot_id' => "B11351ed56d445b0c9417c928662bed48",
                "key_search" => $email
            ]
        ]);
        $resToJson = json_decode($res->getBody()->getContents());
        return $resToJson;
        } catch (GuzzleException $e) {
        return (object) ['status' => 'fail'];
        }
    }

    private function sendMessage($msg, $onechat_id)
    {
    try {
        $client = new Client();
        $res = $client->request('POST', "https://chat-public.one.th:8034/api/v1/push_message", [
            "headers" => [
                'Authorization' => "Bearer A34b3653754575acb8358883ba2df6e56e81eae7b4cc14c1d9d773f79e24a00a59fd5fb165e0a422aaea80d0eea2e990b",
                "Content-Type" => "application/json",
            ],
            'json' => [
                'to' => $onechat_id,
                'bot_id' => "B11351ed56d445b0c9417c928662bed48",
                'type' => 'text',
                "message" => $msg,
            ]
        ]);
        return null;
        } catch (GuzzleException $e) {
        return null;
        }
    }
}
