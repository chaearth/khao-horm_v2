<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class One_chat extends Model
{
    protected $table = 'one_chat';
    protected $fillable = ['one_mail','onechat_id','user_id'];
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
