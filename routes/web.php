<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::get('/', function () {
  return view('productWelcome.product_welcome');
});

Route::get('/', 'ProductController@getProduct')->name('productWelcome.product_welcome');
Route::get('/product', 'ProductController@index')->name('product.page');

Route::get('/product/create', 'ProductController@createPage')->name('product.create.page');
Route::post('/product/create', 'ProductController@create')->name('product.create');

Route::get('/register', 'User\UserController@registerPage')->name('register.page');
Route::post('/register', 'User\UserController@register')->name('register');

Route::get('/login', 'User\UserController@loginPage')->name('login.page');
Route::post('/login', 'User\UserController@login')->name('login');

Route::post('/logout', 'User\UserController@logout')->name('logout');

Route::get('/edit/{product}', 'ProductController@editPage')->name('product.edit.page');
Route::post('/edit', 'ProductController@edit')->name('product.edit');

Route::get('/product/delete/{id}', 'ProductController@deletePage')->name('product.delete');

Route::post('/logout', 'User\UserController@logout')->name('logout');

Route::get('/productuser', 'ProductController@getProduct2')->name('productWelcome.product_admin');
Route::get('/cart', 'ProductController@cart');
Route::get('add-to-cart/{id}', 'ProductsController@addToCart');
Route::patch('update-cart', 'ProductsController@update');
Route::delete('remove-from-cart', 'ProductsController@remove');

Route::delete('remove-from-cart', 'ProductsController@remove');

Route::get('/adminIndex', 'User\UserController@adminIndexPage')->name('adminIndex.page');

Route::get('/profile', 'User\UserController@profile')->name('profile');
Route::post('/onechat/store', 'User\UserController@storeOnechat')->name('onechat.store');

//Route for normal user
Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', 'HomeController@index');
    Route::get('/orders', 'OrderController@showOrders')->name('getOrders');
    Route::get('/confirmOrder/{email}', 'OrderController@confirmOrder')->name('confirmOrder');
   
});
//Route for admin
Route::group(['prefix' => 'admin'], function () {
    Route::group(['middleware' => ['admin']], function () {
        Route::get('/dashboard', 'admin\AdminController@index');
    });
});
